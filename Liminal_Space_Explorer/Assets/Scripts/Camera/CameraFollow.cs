using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _spaceShipTransform;
    [SerializeField][Range (0f, 0.5f)] private float _lerpLag;
    [SerializeField] private float _cameraDistance;

    void FixedUpdate()
    {
        Vector3 _targetPosition = _spaceShipTransform.position + new Vector3(0, 0, _cameraDistance);
        //Vector3 _lerpPosition = Vector3.Lerp(transform.position, _targetPosition, _lerpLag);
        transform.position = _targetPosition;
    }
}
