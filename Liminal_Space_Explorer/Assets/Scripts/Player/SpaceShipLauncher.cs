using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpaceShipLauncher : MonoBehaviour
{
    [Header ("References")]
    [SerializeField] private Transform _spaceShipTransform;
    [SerializeField] private Rigidbody _spaceShipRigidBody;
    [SerializeField] private MouseHandler _mouseHandler;
    [SerializeField] private SpaceShipMovement _spaceShipMovement;

    [Header("Customissation")]
    [SerializeField][Range (1, 5)] public float _slingShotPowerMultiplier;
    [SerializeField] public float _slingShotVectorMagnitudeMax;

    private bool _isMouseDown;
    private Vector3 _mouseWorldPosition;
    private Vector3 _slingShotVector;

    public delegate void Event();
    public static event Event _armingEvent;
    public static event Event _releaseEvent;

    private void Update()
    {
        _isMouseDown = _mouseHandler.GetMouseDown();
        _mouseWorldPosition = _mouseHandler.GetMouseWorldPosition();
        if (_isMouseDown)
        {
            _mouseHandler.UpdateMouseWorldPosition();
            SlingShotArming();
        }
    }

    private void FixedUpdate()
    {
        if (!_isMouseDown)
        {
            _spaceShipMovement.UpdateLookRotation();
            //_spaceShipMovement.UpdateTilt();
            //_spaceShipMovement.LimitRotation();
        }
        else
        {
            _spaceShipMovement.UpdateLookRotationArming(_slingShotVector);
        }
    }

    private void OnMouseDown()
    {
        // Invoke Unity Event
        _armingEvent.Invoke();
    }

    private void OnMouseUp()
    {
        if (_isMouseDown)
        {
            SlingShortRelease();
        }
    }

    private void SlingShotArming()
    {
        //Clean previous speed and forces
        _spaceShipRigidBody.velocity = Vector3.zero;
        _spaceShipRigidBody.angularVelocity = Vector3.zero;

        //Process the world mouse position
        _slingShotVector = Vector3.ClampMagnitude(_spaceShipTransform.position - _mouseWorldPosition, _slingShotVectorMagnitudeMax);
        _slingShotVector.z = 0;
    }

    private void SlingShortRelease()
    {
        //Invoke Unity Event
        _releaseEvent.Invoke();

        _spaceShipRigidBody.AddForce(Vector3.ClampMagnitude(_slingShotVector, _slingShotVectorMagnitudeMax) * _slingShotPowerMultiplier, ForceMode.Impulse);
    }

    public Vector3 GetSlingShotVector()
    {
        return Vector3.ClampMagnitude(_slingShotVector, _slingShotVectorMagnitudeMax);
    }

    public float GetSlingShotVectorMagnitude()
    {
        return Vector3.ClampMagnitude(_slingShotVector, _slingShotVectorMagnitudeMax).magnitude;
    }

    public float GetSlingShotVectorMagnitudeMax()
    {
        return _slingShotVectorMagnitudeMax;
    }
}
