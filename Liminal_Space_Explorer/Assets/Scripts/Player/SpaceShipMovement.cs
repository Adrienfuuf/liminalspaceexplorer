using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class SpaceShipMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody _spaceShipRigidBody;
    [SerializeField] private GameObject _spaceShip3dModel;
    [SerializeField] private AnimationCurve _animationCurve;
    [SerializeField] private float _maxSpaceShipSpeedAnimationCurve;

    private float _lastZRotation;
    private float _deltaRotation;

    private void Update()
    {
        _lastZRotation = transform.rotation.z;
    }

    public void UpdateLookRotation()
    {
        Vector3 relativePos = this._spaceShipRigidBody.velocity;
        float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void UpdateLookRotationArming(Vector3 ArmingVector)
    {
        Vector3 relativePos = ArmingVector;
        float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void UpdateTilt()
    {
        _deltaRotation = (this.transform.rotation.z - _lastZRotation) * 100;
        Debug.Log(this._spaceShipRigidBody.velocity.magnitude);
        float tilt = _animationCurve.Evaluate(_deltaRotation / _maxSpaceShipSpeedAnimationCurve);
        Debug.Log(tilt);
        _spaceShip3dModel.transform.localRotation = Quaternion.Euler(180 * tilt - 90, _spaceShip3dModel.transform.localRotation.y, _spaceShip3dModel.transform.localRotation.z);
    }

    public void LimitRotation()
    {
        Vector3 pea = _spaceShip3dModel.transform.localRotation.eulerAngles;
        pea.x = (pea.x > 180) ? pea.x - 360 : pea.x;
        pea.x = Mathf.Clamp(pea.x, -90, 90);
        _spaceShip3dModel.transform.localRotation = Quaternion.Euler(pea);
    }
}
