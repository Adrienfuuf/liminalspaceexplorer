using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipCrash : MonoBehaviour
{
    public delegate void Event();
    public static event Event _spaceShipCrashEvent;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Planet")
        {
            if(_spaceShipCrashEvent != null)
            {
                _spaceShipCrashEvent.Invoke();
            }
        }
    }
}
