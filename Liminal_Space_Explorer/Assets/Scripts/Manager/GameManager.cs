using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;

    private void EndRound()
    {
        Debug.Log("Game Is Over, Score : " + scoreManager.GetScore().ToString());
    }

    private void OnEnable()
    {
        SpaceShipCrash._spaceShipCrashEvent += EndRound;
    }

    private void OnDisable()
    {
        SpaceShipCrash._spaceShipCrashEvent -= EndRound;
    }
}
