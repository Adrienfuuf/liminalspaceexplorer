using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTest : MonoBehaviour
{
    void OnEnable()
    {
        SpaceShipLauncher._armingEvent += ArmingDebug;
        SpaceShipLauncher._releaseEvent += ReleaseDebug;
    }

    private void ArmingDebug()
    {
        Debug.Log("Arming");
    }

    private void ReleaseDebug()
    {
        Debug.Log("Release");
    }

}
