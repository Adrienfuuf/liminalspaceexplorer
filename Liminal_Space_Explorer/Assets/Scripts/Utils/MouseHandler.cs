using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseHandler : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;
    private Vector3 _mouseWorldPosition;
    private bool _isMouseDown;

    public bool GetMouseDown()
    {
        return _isMouseDown;
    }

    public Vector3 GetMouseWorldPosition()
    {
        return _mouseWorldPosition;
    }

    public void UpdateMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = _mainCamera.nearClipPlane;
        _mouseWorldPosition = _mainCamera.ScreenToWorldPoint(mousePosition);
    }

        private void OnMouseDown()
    {
        _isMouseDown = true;
    }

    private void OnMouseUp()
    {
        _isMouseDown = false;
    }
}
