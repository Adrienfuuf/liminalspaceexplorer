using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private int _score;
    [SerializeField] private TMP_Text _scoreUiText;

    public delegate void Event();
    public static event Event _scoreChangeEvent;

    public void AddToScore(int _pointsToAdd)
    {
        _score += _pointsToAdd;
        _scoreChangeEvent.Invoke();
    }

    public void SubtractToScore(int _pointsToSubtract)
    {
        if(_score - _pointsToSubtract <= 0 )
        {
            _score = 0;
        }
        else
        {
            _score -= _pointsToSubtract;
        }
    }

    private void UpdateScoreUi()
    {
        _scoreUiText.text = _score.ToString();
    }

    public int GetScore()
    {
        return _score;
    }

    private void OnEnable()
    {
        _scoreChangeEvent += UpdateScoreUi;
    }

    private void OnDisable()
    {
        _scoreChangeEvent -= UpdateScoreUi;
    }
}
