using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollector : MonoBehaviour
{
    private ParticleSystem _particleSystem;
    public List<ParticleSystem.Particle> _particles = new List<ParticleSystem.Particle>();
    [SerializeField] private PlanetScore _planetScore;

    private void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    private void OnParticleTrigger()
    {
        int triggeredParticles = _particleSystem.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, _particles);
        for (int i = 0; i < triggeredParticles; i++)
        {
            ParticleSystem.Particle p = _particles[i];
            p.remainingLifetime = 0;
            _particles[i] = p;
            _planetScore.DistributePoints();
        }

        _particleSystem.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, _particles);
    }
}
