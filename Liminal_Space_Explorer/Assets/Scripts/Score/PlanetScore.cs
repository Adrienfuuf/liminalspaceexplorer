using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class PlanetScore : MonoBehaviour
{
    //This Script handles planet points available and when/how to give them to the player
    [Header("References")]
    [SerializeField] private ScoreManager _scoreManager;
    [SerializeField] private TMP_Text _remainingPointsUiText;
    [SerializeField] private ParticleSystem _planetRessourcesParticles;
    [SerializeField] private ParticleCollector _particleCollector;
    [Header("Customization")]
    [SerializeField] private int _planetPointsAvailable;
    [SerializeField] private int _scoreUnit;
    [SerializeField] private float _timeBetweenGivePoints;

    private bool _canGivePoints;
    private Transform _spaceShipTransform;
    private bool _canPlayParticles;
    private void Start()
    {
        // FindSpaceShip and give it to particle
        _spaceShipTransform = FindObjectOfType<SpaceShipLauncher>().gameObject.transform;
        _planetRessourcesParticles.trigger.AddCollider(_spaceShipTransform);

        // Get ScoreManager
        _scoreManager = FindObjectOfType<ScoreManager>();

        _canGivePoints = true;

        // Set UI
        _remainingPointsUiText.text = _planetPointsAvailable.ToString();
    }

    private void Update()
    {
        if (_planetRessourcesParticles.isPlaying && _planetPointsAvailable <= 0)
        {
            DisablePlanetParticles();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        _canPlayParticles = true;
        EnablePlanetParticles();    
    }

    private void OnTriggerExit(Collider other)
    {
        _canPlayParticles = false;
        DisablePlanetParticles();
    }

    public void DistributePoints()
    {
        if (_canGivePoints)
        {
            if (_planetPointsAvailable <= 0)
            {
                return;
            }
            else
            {
                _scoreManager.AddToScore(_scoreUnit);
                _planetPointsAvailable -= _scoreUnit;
                UpdatePlanetPointsUI();
            }
        }
    }

    public void EnablePlanetParticles()
    {
        if(_planetPointsAvailable >= 0 && _canPlayParticles)
        {
            if (!_planetRessourcesParticles.isPlaying)
            {
                _planetRessourcesParticles.Play();
            }
        }
    }

    private void DisablePlanetParticles()
    {
        if (_planetRessourcesParticles.isPlaying)
        {
            _planetRessourcesParticles.Stop();
        }
    }

    private void UpdatePlanetPointsUI()
    {
        _remainingPointsUiText.text = _planetPointsAvailable.ToString();
    }

    private void DisableGivePoints()
    {
        _canGivePoints = false;
    }

    private void EnableGivePoints()
    {
        _canGivePoints = true;
    }

    private void OnEnable()
    {
        SpaceShipLauncher._armingEvent += DisableGivePoints;
        SpaceShipLauncher._armingEvent += DisablePlanetParticles;
        SpaceShipLauncher._releaseEvent += EnableGivePoints;
        SpaceShipLauncher._releaseEvent += EnablePlanetParticles;
    }

    private void OnDisable()
    {
        SpaceShipLauncher._armingEvent -= DisableGivePoints;
        SpaceShipLauncher._releaseEvent -= EnableGivePoints;
    }
}

