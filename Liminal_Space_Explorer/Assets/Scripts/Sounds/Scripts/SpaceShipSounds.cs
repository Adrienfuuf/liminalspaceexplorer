using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipSounds : MonoBehaviour
{
    [SerializeField]  private AudioSource _spaceShipReleaseSound;
    [SerializeField]  private AudioSource _spaceShipArmingSound;
    
    private void PlayReleaseSound()
    {
        _spaceShipReleaseSound.Play();
    }

    private void PlayArmingSound()
    {
      _spaceShipArmingSound.Play();
    }

    private void StopArmingSound()
    {
        _spaceShipArmingSound.Stop();
    }

    void OnEnable()
    {
        SpaceShipLauncher._releaseEvent += PlayReleaseSound;
        SpaceShipLauncher._releaseEvent += StopArmingSound;
        SpaceShipLauncher._armingEvent += PlayArmingSound;
    }

    void OnDisable()
    {
        SpaceShipLauncher._releaseEvent -= PlayReleaseSound;
        SpaceShipLauncher._releaseEvent -= StopArmingSound;
        SpaceShipLauncher._armingEvent -= PlayArmingSound;
    }

}
