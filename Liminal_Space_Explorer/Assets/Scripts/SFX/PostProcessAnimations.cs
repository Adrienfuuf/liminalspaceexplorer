using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessAnimations : MonoBehaviour
{
    [SerializeField] Volume _globalVolume;
    [SerializeField] SpaceShipLauncher _spaceShipLauncher;

    [Header ("Release Animation")]
    [SerializeField] private AnimationCurve _lenseDistortionAnimationCurve;
    [SerializeField] private float _animationDurationInSeconds;
    [SerializeField] private float _timeBeforeAnimation;
    private LensDistortion _lenseDistortion;
    private float _slingShotMagnitude;
    private float _slingShotMagnitudeMax;

    private void OnEnable()
    {
        SpaceShipLauncher._armingEvent += ArmingAnimation;
        SpaceShipLauncher._releaseEvent += ReleaseLensAnimation;
    }

    private void OnDisable()
    {
        SpaceShipLauncher._armingEvent -= ArmingAnimation;
        SpaceShipLauncher._releaseEvent -= ReleaseLensAnimation;
    }

    private void ReleaseLensAnimation()
    {
        StartCoroutine(ReleaseLensAnimationCoroutine());
    }

    private IEnumerator ReleaseLensAnimationCoroutine()
    {
        _slingShotMagnitude = _spaceShipLauncher.GetSlingShotVectorMagnitude();
        _slingShotMagnitudeMax = _spaceShipLauncher.GetSlingShotVectorMagnitudeMax();
        float _startTime = Time.time;
        float duration = 0;
        float powerLevel = 0;
        _globalVolume.profile.TryGet(out _lenseDistortion);
        yield return new WaitForSeconds(_timeBeforeAnimation);
        while (duration < _animationDurationInSeconds)
        {
            powerLevel = Mathf.Clamp((_slingShotMagnitude / Mathf.Pow(_slingShotMagnitudeMax, 0.8f)), 0, 1);
            _lenseDistortion.intensity.value = powerLevel * ((2 * _lenseDistortionAnimationCurve.Evaluate(duration) - 1) * _spaceShipLauncher.GetSlingShotVector().normalized.magnitude);
            duration += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        _lenseDistortion.intensity.value = 0;
    }

    private void ArmingAnimation()
    {

    }
}
