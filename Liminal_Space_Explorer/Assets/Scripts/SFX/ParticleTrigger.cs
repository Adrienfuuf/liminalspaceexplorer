using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTrigger : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleToTrigger;

    private void TriggerParticles()
    {
        _particleToTrigger.Play();
    }

    private void OnEnable()
    {
        SpaceShipLauncher._releaseEvent += TriggerParticles;
    }

    private void OnDisable()
    {
        SpaceShipLauncher._releaseEvent -= TriggerParticles;
    }
}
