using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipParticles : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private SpaceShipLauncher _spaceShipLauncher;
    [SerializeField] private ParticleSystem _particleSystemAbsorb;

    [Header("Customisation")]
    [SerializeField] private float _particuleRadialMaxSpeed;
    [SerializeField] private float _particuleRadialMaxEmissionRate;
    [SerializeField] private float _emissionRateMultiplier;
    [SerializeField] private float _emissionRateBase;
    private float _slingShotVectorMagnitude;
    private float _slingShotVectorMagnitudeMax;

    private void Start()
    {
        UpdateSlingShotVectorMagnitude();
        _slingShotVectorMagnitudeMax = _spaceShipLauncher.GetSlingShotVectorMagnitudeMax();
    }

    private void Update()
    {
        UpdateSlingShotVectorMagnitude();
        if (_particleSystemAbsorb.isPlaying)
        {
            AdjustParticulesWithSlingShortVector();
        }
    }

    private void AdjustParticulesWithSlingShortVector()
    {
        var velocityOverLifetime = _particleSystemAbsorb.velocityOverLifetime;
        velocityOverLifetime.radial = -_slingShotVectorMagnitude * ( _particuleRadialMaxSpeed / _slingShotVectorMagnitudeMax );

        var emission = _particleSystemAbsorb.emission;
        emission.rateOverTime = _slingShotVectorMagnitude * (_slingShotVectorMagnitudeMax / _particuleRadialMaxEmissionRate) * _emissionRateMultiplier + _emissionRateBase;
    }

    private void UpdateSlingShotVectorMagnitude()
    {
        _slingShotVectorMagnitude = _spaceShipLauncher.GetSlingShotVectorMagnitude();
    }

    public void ToggleParticleAbsorb()
    {
        if (_particleSystemAbsorb.isPlaying)
        {
            _particleSystemAbsorb.Stop();
        }
        else
        {
            _particleSystemAbsorb.Play();
        }
    }

    private void OnEnable()
    {
        SpaceShipLauncher._armingEvent += ToggleParticleAbsorb;
        SpaceShipLauncher._releaseEvent += ToggleParticleAbsorb;
    }

    private void OnDisable()
    {
        SpaceShipLauncher._armingEvent -= ToggleParticleAbsorb;
        SpaceShipLauncher._releaseEvent -= ToggleParticleAbsorb;
    }
}
