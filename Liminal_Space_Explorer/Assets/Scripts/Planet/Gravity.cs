using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    [SerializeField] private float _planetMass;
    [Header ("Gravity Settings")]
    [SerializeField][Range (0, 200)] private float _minimumAttractionDistance;
    [SerializeField][Range(0, 5)] private float _gravitationalConstant;
    [SerializeField][Range(0, 2)] private float _gravityPowerRepartition;
    [Space (10)]
    [Header("Repultion Settings")]
    [SerializeField] private float _minimumRepultionDistance;
    [SerializeField] private float _repultionConstant;
    [SerializeField] private float _repultionPowerRepartition;
    private Rigidbody _spaceShipRb;
    private Vector3 _direction = new Vector3();
    float _distance = 0; 

    private void Start()
    {
        _spaceShipRb = FindObjectOfType<SpaceShipLauncher>().gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float distanceFromPlanet = Vector3.Distance(_spaceShipRb.position, this.transform.position);
        if (distanceFromPlanet < _minimumAttractionDistance)
        {
            _direction = this.transform.position - _spaceShipRb.transform.position;
            _distance = _direction.magnitude;

            ApplyGravity();

            if (Vector3.Distance(_spaceShipRb.position, this.transform.position) < _minimumRepultionDistance)
            {
                ApplyRepultion();
            }
        }
    }

    private void ApplyGravity()
    {
        float forceMagnitude = (_planetMass * _spaceShipRb.mass) / Mathf.Pow(_distance, _gravityPowerRepartition);
        _spaceShipRb.AddForce(_direction.normalized * forceMagnitude * _gravitationalConstant);
    }

    private void ApplyRepultion()
    {
        float forceMagnitude = (_planetMass * _spaceShipRb.mass) / Mathf.Pow(_distance, _repultionPowerRepartition);
        _spaceShipRb.AddForce(-_direction.normalized * forceMagnitude * _repultionConstant);
    }
}
